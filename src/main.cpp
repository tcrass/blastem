#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <ESP8266Audio.h>
#include <LittleFS.h>
#include <AudioFileSourceLittleFS.h>
#include <AudioGeneratorMP3.h>
#include <AudioGeneratorWAV.h>
#include <AudioOutputI2S.h>

// --- Definitions and declarations -----------------------------------

// --- Mode ---

const int BLAST_MODE = 0;
const int STUN_MODE = 1;

const int MAX_MODE = STUN_MODE;

int current_mode;

// --- Audio ---

#define I2S_DI_PIN 3    // RX
#define I2S_BCLK_PIN 15 // D8
#define I2S_LRC_PIN 2   // D4

#define AUDIO_GAIN 2

const char *BLAST_SOUND_FILE = "/blast.mp3";
const char *STUN_SOUND_FILE = "/stun.mp3";

AudioFileSource *blast_sound_file;
AudioGenerator *audio_player;
AudioOutput *audio_out;

char *current_sound_file;
boolean start_new_sound_file = false;
char *new_sound_file;

// --- LED strip ---

#define LED_STRIP_PIN 13 // D7
#define TOTAL_LED_COUNT 37

const int LED_COUNT = int(TOTAL_LED_COUNT / 2.0 + 0.5);

const int LED_DELAY_MS = 4;

const float LED_DECAY_FACTOR = 16;

const float INITIAL_COLOR[3] = {64, 64, 64};
const float BLAST_COLOR[3] = {255, 0, 0};
const float STUN_COLOR[3] = {0, 0, 255};

Adafruit_NeoPixel led_strip(TOTAL_LED_COUNT, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800);

float led_colors[TOTAL_LED_COUNT][3];

float current_led_color[3];

long last_led_update_timestamp = 0;

float led_decay[3] = {0, 0, 0};

// --- Input ---

#define TRIGGER_BUTTON_PIN 0      // D3
#define MODE_SELECT_BUTTON_PIN 14 // D5

#define BUTTON_DEBOUNDING_DELAY_MS 20

long last_trigger_button_timestamp = 0;
long last_mode_select_button_timestamp = 0;

// --- Helper functions -----------------------------------------------

void play_audio(const char *filename)
{
  start_new_sound_file = true;
  new_sound_file = (char *)filename;
}

void push_led_colors()
{
  for (int pixel = 0; pixel < LED_COUNT; pixel++)
  {
    led_strip.setPixelColor(
        pixel,
        static_cast<unsigned int>(led_colors[pixel][0]),
        static_cast<unsigned int>(led_colors[pixel][1]),
        static_cast<unsigned int>(led_colors[pixel][2]));
    led_strip.setPixelColor(
        TOTAL_LED_COUNT - pixel - 1,
        static_cast<unsigned int>(led_colors[pixel][0]),
        static_cast<unsigned int>(led_colors[pixel][1]),
        static_cast<unsigned int>(led_colors[pixel][2]));
  }
  led_strip.show();
}

void propagate_led_colors()
{
  for (int pixel = LED_COUNT; pixel > 0; pixel--)
  {
    for (int c = 0; c < 3; c++)
    {
      led_colors[pixel][c] = led_colors[pixel - 1][c];
    }
  }
  for (int c = 0; c < 3; c++)
  {
    if (led_colors[0][c] >= led_decay[c])
    {
      led_colors[0][c] -= led_decay[c];
    }
    else
    {
      led_colors[0][c] = 0;
    }
  }
  push_led_colors();
}

void set_led_color(const float colors[3])
{
  for (int c = 0; c < 3; c++)
  {
    led_colors[0][c] = colors[c];
    led_decay[c] = colors[c] / LED_DECAY_FACTOR;
  }
  push_led_colors();
}

void initialize_led_colors()
{
  for (int pixel = 0; pixel < TOTAL_LED_COUNT; pixel++)
  {
    for (int c = 0; c < 3; c++)
    {
      led_colors[pixel][c] = INITIAL_COLOR[c];
    }
  }
  set_led_color(INITIAL_COLOR);
}

void set_current_color(const float color[3])
{
  for (int i = 0; i < 3; i++)
  {
    current_led_color[i] = color[i];
  }
}

void apply_mode(int mode)
{
  switch (mode)
  {
  case BLAST_MODE:
    current_sound_file = (char *)BLAST_SOUND_FILE;
    set_current_color(BLAST_COLOR);
    break;
  case STUN_MODE:
    current_sound_file = (char *)STUN_SOUND_FILE;
    set_current_color(STUN_COLOR);
    break;
  default:
    break;
  }
}

// --- Input handlers -------------------------------------------------

ICACHE_RAM_ATTR void onTriggerButton()
{
  long now = millis();
  if (now - BUTTON_DEBOUNDING_DELAY_MS > last_trigger_button_timestamp)
  {
    last_trigger_button_timestamp = now;
    Serial.println("* Trigger button pressed!");
    set_led_color(current_led_color);
    play_audio(current_sound_file);
  }
}

ICACHE_RAM_ATTR void onModeSelectButton()
{
  long now = millis();
  if (now - BUTTON_DEBOUNDING_DELAY_MS > last_mode_select_button_timestamp)
  {
    last_mode_select_button_timestamp = now;
    Serial.println("* Mode select button pressed!");
    current_mode++;
    if (current_mode > MAX_MODE)
    {
      current_mode = 0;
    }
    apply_mode(current_mode);
    set_led_color(current_led_color);
  }
}

// --- Setup ----------------------------------------------------------

void setup()
{

  // --- Misc ---

  delay(500);
  Serial.begin(115200);

  if (LittleFS.begin())
  {
    Serial.println("Filesystem successfully mounted.");
  }
  else
  {
    Serial.println("Error mounting filesystem!");
  }

  Serial.println("Files in root directory:");
  Dir root = LittleFS.openDir("/");
  while (root.next())
  {
    Serial.println(root.fileName());
  }
  Serial.println("---");

  apply_mode(current_mode);

  // --- Audio ---

  audio_player = new AudioGeneratorMP3();
  audio_out = new AudioOutputI2S();
  audio_out->SetGain(AUDIO_GAIN);

  // --- LED strip ---

  led_strip.begin();

  initialize_led_colors();

  // --- Input ---

  pinMode(TRIGGER_BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(TRIGGER_BUTTON_PIN, onTriggerButton, FALLING);

  pinMode(MODE_SELECT_BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(MODE_SELECT_BUTTON_PIN, onModeSelectButton, FALLING);
}

// --- Main loop ------------------------------------------------------

void loop()
{

  // --- Misc ---

  // --- Audio ---

  if (start_new_sound_file)
  {
    start_new_sound_file = false;
    if (audio_player->isRunning())
    {
      audio_player->stop();
    }
    AudioFileSource *sound_file = new AudioFileSourceLittleFS(new_sound_file);
    audio_player->begin(sound_file, audio_out);
  }

  if (audio_player->isRunning())
  {
    if (!audio_player->loop())
    {
      audio_player->stop();
    }
  }

  // --- LED strip ---

  long now = millis();
  if (now - LED_DELAY_MS > last_led_update_timestamp)
  {
    last_led_update_timestamp = now;
    propagate_led_colors();
  }
}
