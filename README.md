# Blast'em!

Arduino code for some Sci-Fi cosplay blaster electronics.

## Hardware

The blaster electronics kit is based on an ESP9266 microcontroller. We initially used a NodeMCU board for prototyping, but the final parts list will probably look as follows:

- Adafruit Feather HUZZAH with ESP8266 (with integrated battery charging circuit!)
- A small rechargeable LiPo battery
- Some WS2812B-based programmable RGB LED strip
- Adafruit I2S 3W Class D Amplifier Breakout - MAX98357A
- (Yet to be tested: a 100 Ohm resistor to adjust the amplifier's, well, amplification)
- A small (3 cm) 4 Ohm speaker
- Two push buttons:
  - One for the trigger
  - One for toggling between "blast" and "stun" mode

## Software

We use Visual Studio Code for development, leveraging the PlatformIO extension for microcontroller-specific tasks.

## Resources 

Sounds were designed using the Helm synthesizer plugin in Qtractor, post-processing is performed using Ardour.

## Operating System

All work is, of course, done on Debian Linux! :)
